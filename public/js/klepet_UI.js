function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}
function podporaSmeskov(sporocilo){
  
  sporocilo = sporocilo.replace(/;\)/g, "<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png'>");
  sporocilo = sporocilo.replace(/:\)/g, "<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png'>");
  sporocilo = sporocilo.replace(/\(y\)/g, "<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png'>");
  sporocilo = sporocilo.replace(/:\*/g, "<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png'>");
  sporocilo = sporocilo.replace(/:\(/g, "<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png'>");
  
  return sporocilo;
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    sporocilo = podporaSmeskov(sporocilo);
    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  var vzdevek, kanal;

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
    $('#kanal').text(rezultat.vzdevek+' @ '+ kanal);
    vzdevek = rezultat.vzdevek;
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    $('#kanal').text(vzdevek + ' @ ' + rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
    kanal = rezultat.kanal;
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').text(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});